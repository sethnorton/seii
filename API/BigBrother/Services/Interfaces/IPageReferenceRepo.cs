﻿using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// Interface for the page reference repo
    /// </summary>
    public interface IPageReferenceRepo
    {

        public void CreateEntry(PageReferenceTable pageReference);  //creates an entry in the page reference table
        public PageReferenceTable GetEntry(string pageReference);   //gets an entry in the page reference table
        public void UpdateEntry(PageReferenceTable pageRef);        //updates an entry in a update entry
        public void DeletePageRef(PageReferenceTable pageRefID);    //deletes an entry in the page entry
    }
}
