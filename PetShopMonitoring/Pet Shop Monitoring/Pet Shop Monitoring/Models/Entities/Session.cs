﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pet_Shop_Monitoring.Models.Entities
{
    public class Session
    {
        [Key]
        public int SessionID { get; set; }
        [ForeignKey("IPAddress")]
        public string IpAddress { get; set; }
        public DateTime DateTime { get; set; }
        public bool LoggedIn { get; set; }
        public bool PurchaseMade { get; set; }
    }
}
