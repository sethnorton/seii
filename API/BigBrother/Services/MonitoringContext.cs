﻿using BigBrother.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace BigBrother.Models
{
    //Context class for Pet Shoppppppppp. 
    public class MonitoringContext : DbContext
    {
        public MonitoringContext(DbContextOptions options) : base(options)
        {   

        }
        public DbSet<Interaction> Interaction { get; set; }

        public  DbSet<IPAddress> IPAddress { get; set; }

        public  DbSet<Session> Session { get; set; }    

        public DbSet<PageReferenceTable> PageReferenceTable { get; set; }

    }
}
