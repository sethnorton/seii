﻿using Microsoft.EntityFrameworkCore;
using Pet_Shop_Monitoring.Models.Entities;

namespace Pet_Shop_Monitoring.Services
{
    public class MonitoringDbContext : DbContext
    {
        public MonitoringDbContext(DbContextOptions options) : base(options) { }
        public DbSet<Interaction> Interaction { get; set; }
        public DbSet<IPAddress> IPAddress { get; set; }
        public DbSet<PageReferenceTable> PageReferenceTable { get; set; }
        public DbSet<Session> Session { get; set; }
    }
}
