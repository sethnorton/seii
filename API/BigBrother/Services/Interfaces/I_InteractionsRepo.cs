﻿using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// Interface that contains various abstract methods that define
    /// various actions to perform on the Interaction table within the
    /// database.
    /// </summary>
    public interface I_InteractionsRepo
    {
        /// <summary>
        /// Creates an Interaction entry within the database.
        /// </summary>
        /// <param name="interactionToCreate">Interaction to create an entry for.</param>
        public void CreateEntry(Interaction interactionToCreate);

        /// <summary>
        /// Updates an existing Interaction entry within the database.
        /// </summary>
        /// <param name="interactionToUpdate">Interaction entry to update.</param>
        public void UpdateEntry(Interaction interactionToUpdate);

        /// <summary>
        /// Gets an existing Interaction within the database.
        /// </summary>
        /// <param name="interactionID">ID of the Interaction to get.</param>
        /// <returns>Interaction within the database that matches the specified ID.</returns>
        public Interaction GetEntry(int interactionID);

        /// <summary>
        /// Deletes an existing Interaction within the database.
        /// </summary>
        /// <param name="interactionID">ID of the Interaction to get.</param>
        /// <returns>Interaction within the database that matches the specified ID.</returns>
        public void DeleteEntry(int interactionID);
    }
}