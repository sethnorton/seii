﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Serialization;
using BigBrother.Models.Entities;
using BigBrother.Models;
using BigBrother.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BigBrother.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IPAddressController : ControllerBase
    {
        //This particular controller would not need a delete method. Even if the user did not purchase an item they still accessed the site.
        // It honestly doesnt need to do anything but add an IP and verify the if the user was here at one point to add them. 

        private readonly I_IPAddressRepo _ipRepo;

        //Grants access to the repo.
        public IPAddressController(I_IPAddressRepo managerRepository)
        {
            _ipRepo = managerRepository;
        }


        // takes the IP address of the current web user and makes a call to an external api to get geo-loc data that is then
        //parsed and sent either to be posted to the database for the first time or for the information that we have
        //on the ip address to be update along with an increase in the visit count for that user. 
        [HttpGet("{ip}")]
        public void GetIPAddress(string ip)
        {
            IP_APICleaner ipClean = new IP_APICleaner();

            string cleanResponse = ipClean.GetIPAPIResponse(ip);
            string[] postableData = ipClean.DatabaseReadyData(cleanResponse);

            if (IPExists(ip.Trim(), "1"))
            {
                Update(postableData);
            }
            else
            {
                Post(postableData);

            }

        }

        //Posts a new entry to the data base.
        [HttpPost]
        public void Post(string[] data)
        {
            IPAddress newEntry = new IPAddress();

            newEntry.CountryName = data[0];
            newEntry.CountryCode = data[1];
            newEntry.StateOrRegion = data[2];
            newEntry.City = data[3];
            newEntry.ZipCode = data[4];
            newEntry.DeviceType = data[5];
            newEntry.IpAddress = data[6];
            newEntry.VisitCount = 1;
            _ipRepo.CreateEntry(newEntry);

        }

        // PUT api/<ValuesController>/5
        //Updates an entry if it already exists and requires it. 
        [HttpPut]
        public void Update(string[] data)
        {
            IPAddress updatedEntry = new IPAddress();
            updatedEntry.CountryName = data[0];
            updatedEntry.CountryCode = data[1];
            updatedEntry.StateOrRegion = data[2];
            updatedEntry.City = data[3];
            updatedEntry.ZipCode = data[4];
            updatedEntry.DeviceType = data[5];
            updatedEntry.IpAddress = data[6];
            _ipRepo.UpdateEntry(updatedEntry);

        }


        //Checks to see if an IP has already been logged on the table.  
        [HttpGet("{ipInTable}/{CountryName}")]
        public bool IPExists(string ipInTable, string CountryName)
        {
            var ipList = _ipRepo.ReadAll();

            foreach (var i in ipList)
            {
                if (i.IpAddress == ipInTable)
                {
                    return true;
                }

            }

            return false;
        }


    }
}
