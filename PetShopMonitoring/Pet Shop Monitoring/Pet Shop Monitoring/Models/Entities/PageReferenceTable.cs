﻿using System.ComponentModel.DataAnnotations;

namespace Pet_Shop_Monitoring.Models.Entities
{
    public class PageReferenceTable
    {
        [Key]
        public int PageId { get; set; }
        public DateTime DateAdded { get; set; }
        public string PageDescription { get; set; }
    }
}
