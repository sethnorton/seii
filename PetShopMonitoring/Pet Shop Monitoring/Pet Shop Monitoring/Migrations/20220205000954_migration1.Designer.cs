﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Pet_Shop_Monitoring.Services;

#nullable disable

namespace Pet_Shop_Monitoring.Migrations
{
    [DbContext(typeof(MonitoringDbContext))]
    [Migration("20220205000954_migration1")]
    partial class migration1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("Pet_Shop_Monitoring.Models.Entities.Interaction", b =>
                {
                    b.Property<int>("InteractionID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("InteractionID"), 1L, 1);

                    b.Property<DateTime>("DateAndTime")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("InteractionLength")
                        .HasColumnType("datetime2");

                    b.Property<int>("pageID")
                        .HasColumnType("int");

                    b.Property<int>("sessionID")
                        .HasColumnType("int");

                    b.HasKey("InteractionID");

                    b.ToTable("Interaction");
                });

            modelBuilder.Entity("Pet_Shop_Monitoring.Models.Entities.IPAddress", b =>
                {
                    b.Property<string>("IpAddress")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CountryCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CountryName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("StateOrRegion")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("ZipCode")
                        .HasColumnType("float");

                    b.HasKey("IpAddress");

                    b.ToTable("IPAddress");
                });

            modelBuilder.Entity("Pet_Shop_Monitoring.Models.Entities.PageReferenceTable", b =>
                {
                    b.Property<int>("PageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("PageId"), 1L, 1);

                    b.Property<DateTime>("DateAdded")
                        .HasColumnType("datetime2");

                    b.Property<string>("PageDescription")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("PageId");

                    b.ToTable("PageReferenceTable");
                });

            modelBuilder.Entity("Pet_Shop_Monitoring.Models.Entities.Session", b =>
                {
                    b.Property<int>("SessionID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("SessionID"), 1L, 1);

                    b.Property<DateTime>("DateAndTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("IpAddress")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("LoggedIn")
                        .HasColumnType("bit");

                    b.Property<bool>("PurchaseMade")
                        .HasColumnType("bit");

                    b.Property<DateTime>("SessionLength")
                        .HasColumnType("datetime2");

                    b.HasKey("SessionID");

                    b.ToTable("Session");
                });
#pragma warning restore 612, 618
        }
    }
}
