﻿using System.ComponentModel.DataAnnotations;

namespace BigBrother.Models.Entities
{
    /// <summary>
    /// Class which represents an Interaction entity.
    /// </summary>
    public class Interaction
    {
        [Key]
        public int InteractionID { get; set; } // Public key for the Interaction entity
        public int SessionID { get; set; } // Foreign key containing the SessionID from a Session entity
        public DateTime DateTime { get; set; } // Date and time when a page reference is removed
        public int PageID { get; set; } // Page ID for the interaction
        public DateTime InteractionLength { get; set; } // Time in which an interaction lasted 

        /// <summary>
        /// Override for the default ToString() method.
        /// </summary>
        /// <returns>Formatted string that represents an Interaction entity.</returns>
        public override string ToString()
        {
            return ("ID: " + InteractionID.ToString() + "\nDate & Time: " + DateTime.ToString() + "\nInteraction Length: " + InteractionLength.ToString()); // Return a formatted string that represents an Interaction entity
        }
    }
}