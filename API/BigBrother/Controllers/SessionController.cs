﻿using BigBrother.Models.Entities;
using BigBrother.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BigBrother.Controllers
{
    /// <summary>
    /// A controller which deals with getting, posting and putting data in the session repo
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : Controller
    {
        private readonly ISessionsRepo sessionRepo; //variable which saves the session info to the database


        /// <summary>
        /// The constructor for the session controller which initiallizes a variable to store information in the session table
        /// </summary>
        /// <param name="sessionRepo">The variable used to put, get, post and delete data in the database</param>
        public SessionController(ISessionsRepo sessionRepo)
        {
            this.sessionRepo = sessionRepo;
        }


        /// <summary>
        /// Gets information from a table that contains a certain id, ipAddress, dateTime, logIn and purchased
        /// </summary>
        /// <param name="id">the id for the session</param>
        /// <param name="ipAddress">the ip associated with a session</param>
        /// <param name="dateTime">the dateTime associated with a session</param>
        /// <param name="logIn">Tells whether the user is logged in</param>
        /// <param name="purchased">T</param>
        /// <returns></returns>
        [HttpGet("{id:int}/{ipAddress}/{dateTime}/{logIn:bool}/{purchased:bool}")]
        public IEnumerable<string> Get(int id, string ipAddress, string dateTime, bool logIn, bool purchased)
        {
            dateTime = dateTime.Replace("%2F", "/");
            return new string[] { id.ToString(), ipAddress, dateTime.ToString(), logIn.ToString(), purchased.ToString() };
        }


        /// <summary>
        /// Gets a session by a particular session ID
        /// </summary>
        /// <param name="sessionID">A session ID which identifies a session for a particular user</param>
        /// <returns>A</returns>
        [HttpGet("{sessionID:int}")]
        public string Get(int sessionID)
        {
             Session data = sessionRepo.GetEntry(sessionID.ToString());
             return (data.SessionId + "" + data.IpAddress + "" + data.DateTime + "" + data.LoggedIn + ""+ data.PurchaseMade );
        }

        /// <summary>
        /// Posts a new entry into the database 
        /// </summary>
        /// <param name="data"></param>
        [HttpPost]
        public void Post(string[] data)
        {
            Session s = new Session();
            s.DateTime = DateTime.Parse(data[0]);
            s.LoggedIn = bool.Parse(data[1]);
            s.PurchaseMade = bool.Parse(data[2]);

            if (ModelState.IsValid)
            {
                sessionRepo.CreateEntry(s);
            }
        }

        /// <summary>
        /// Puts a new entry into the session table
        /// </summary>
        /// <param name="dateAdded">When the session was added</param>
        /// <param name="logIn">Is the user logged in</param>
        /// <param name="purchased">Has the user purchased anything in this session</param>
        [HttpPut("{dateAdded}/{logIn:bool}/{purchased:bool}")]
        public void Put(string dateAdded, bool logIn, bool purchased)
        {
            Session s = new Session();
            dateAdded = dateAdded.Replace("%2F", "/");
            s.DateTime = DateTime.Parse(dateAdded);
            s.LoggedIn = logIn;
            s.PurchaseMade = purchased;
            sessionRepo.CreateEntry(s);
        }

        /// <summary>
        /// Deletes an entry by id in the table 
        /// </summary>
        /// <param name="id">the id we want to delete</param>
        [HttpDelete("{id:int}")]
        public void Delete(int id)
        {
            Session s = new Session();
            s.SessionId = id;
            sessionRepo.DeleteSessionRef(s);
        }
    }
}
