﻿using System.ComponentModel.DataAnnotations;

namespace BigBrother.Models.Entities
{


    /// <summary>
    /// Class declaring properties for the attributes in the IPAddress table
    /// </summary>
    public class IPAddress
    {
        [Key]
        public string IpAddress { get; set; }
        public string CountryCode { get; set; }
        public string? CountryName { get; set; }
        public string? StateOrRegion { get; set; }
        public string? City { get; set; }
        public string ZipCode { get; set; }
        public int VisitCount { get; set; }
        public string? DeviceType { get; set; }


    }
}
