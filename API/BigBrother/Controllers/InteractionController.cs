﻿using BigBrother.Services;
using BigBrother.Models.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BigBrother.Controllers
{
    /// <summary>
    /// Class which handles the various HTTP statements for
    /// the Interaction table within the database.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class InteractionController : ControllerBase
    {
        private readonly I_InteractionsRepo interactionsRepo;

        /// <summary>
        /// Parameterized constructor for the InteractionController class.
        /// </summary>
        /// <param name="interactionsRepo"></param>
        public InteractionController(I_InteractionsRepo interactionsRepo)
        {
            this.interactionsRepo = interactionsRepo;
        }

        /// <summary>
        /// Method to handle a HTTP GET statement to get the ID of
        /// a specified interaction.
        /// </summary>
        /// <param name="interactionToGetID">ID of the Interaction to get.</param>
        /// <returns>Existing Interaction within the database.</returns>
        [HttpGet("{interactionToGetID:int}")]
        public string GetInteraction(int interactionToGetID)
        {
            return interactionsRepo.GetEntry(interactionToGetID).ToString(); // Get an existing Interaction within the database
        }

        /// <summary>
        /// Method to handle a HTTP POST statement to create
        /// a new Interaction entry within the database.
        /// </summary>
        /// <param name="dataToPost">String array which contains the data to post within the Interaction entry.</param>
        [HttpPost]
        public void PostInteraction(string[] dataToPost)
        {
            Interaction interactionToPost = new Interaction();

            interactionToPost.DateTime = DateTime.Parse(dataToPost[0]);
            interactionToPost.InteractionLength = DateTime.Parse(dataToPost[1]);

            if (ModelState.IsValid)
            {
                interactionsRepo.CreateEntry(interactionToPost); // Create an Interaction entry within the database.
            }
        }

        /// <summary>
        /// Method to handle a HTTP PUT statement to update
        /// an existing Interaction entry within the database.
        /// </summary>
        /// <param name="interactionID">ID of the Interaction to update.</param>
        /// <param name="dateTime">Updated date and time field.</param>
        /// <param name="interactionLength">Updated interaction length field.</param>
        [HttpPut("{interactionID:int}/{dateTime:datetime}/{interactionLength:datetime}")]
        public void PutInteraction(int interactionID, DateTime dateTime, DateTime interactionLength)
        {
            Interaction interactionToUpdate = new Interaction();

            interactionToUpdate.InteractionID = interactionID;
            interactionToUpdate.DateTime = dateTime;
            interactionToUpdate.InteractionLength = interactionLength;

            interactionsRepo.UpdateEntry(interactionToUpdate); // Update an existing Interaction entry within the database
        }

        /// <summary>
        /// Method to handle a HTTP DELETE statement to delete
        /// an existing Interaction entry from the database.
        /// </summary>
        /// <param name="interactionToDeleteID">ID of the Interaction to delete</param>
        [HttpDelete("{interactionToDeleteID:int}")]
        public void DeleteInteraction(int interactionToDeleteID)
        {
            interactionsRepo.DeleteEntry(interactionToDeleteID); // Delete an existing Interaction from the database
        }
    }
}