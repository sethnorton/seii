﻿using BigBrother.Models;
using BigBrother.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace BigBrother.Services
{
    /// <summary>
    /// The class dealing with storing and retrieving data from the page reference table in the database
    /// </summary>
    public class PageReferenceRepo : IPageReferenceRepo
    {
        private readonly MonitoringContext _db;

        /// <summary>
        /// The constructor for the page reference repo. Initializes a monitoring context object which helps interact with the database
        /// </summary>
        /// <param name="db">The database context object which we can update</param>
        public PageReferenceRepo(MonitoringContext db)
        {
            _db = db;

        }

       /// <summary>
       /// Creates a new entry in the page reference table
       /// </summary>
       /// <param name="pageReference">The object representing a page reference</param>
        public void CreateEntry(PageReferenceTable pageReference)
        {
            _db.PageReferenceTable.Add(pageReference);
            
            _db.SaveChanges();
           
        }


        /// <summary>
        /// Gets an entry in the database depending on the ID entered
        /// </summary>
        /// <param name="pageReferenceID">The id of the page reference</param>
        /// <returns></returns>
        public PageReferenceTable GetEntry(string pageReferenceID)
        {
            
            return _db.PageReferenceTable.FirstOrDefault(i => i.PageId == Int32.Parse(pageReferenceID));

        }


        /// <summary>
        /// Updates an entry in the page reference table
        /// </summary>
        /// <param name="pageRef">The page reference which we are updating</param>
        public void UpdateEntry(PageReferenceTable pageRef)
        {
            var pageReferenceEntry = GetEntry(pageRef.PageId.ToString());
            pageReferenceEntry.DateAdded = pageRef.DateAdded;
            pageReferenceEntry.PageDescription = pageRef.PageDescription;
            _db.SaveChanges();

        }

        /// <summary>
        /// Deletes a page reference in the code 
        /// </summary>
        /// <param name="pageRefID">The page reference table</param>
        public void DeletePageRef(PageReferenceTable pageRefID)
        {
            var result = GetEntry(pageRefID.PageId.ToString());
            _db.PageReferenceTable.Remove(pageRefID);
            _db.SaveChanges();
        }

    }
}
