﻿using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// The interface for the session repo which deals with data for the database
    /// </summary>
    public interface ISessionsRepo
    {
        public void CreateEntry(Session session);
        public Session GetEntry(string pageReference);
        public void UpdateEntry(Session pageRef);
        public void DeleteSessionRef(Session pageRefID);
    }
}
