﻿using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// The IP address repository which deals with creating entries, updating entries, reading all the entries and getting the entry
    /// </summary>
    public interface I_IPAddressRepo
    {
        public ICollection<IPAddress> ReadAll();

        public void CreateEntry(IPAddress ipInfo);

        public void UpdateEntry(IPAddress ipInfo);

        public IPAddress GetEntry(string ipAddress);
        


    }
}
