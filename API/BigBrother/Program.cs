using Microsoft.EntityFrameworkCore;
using BigBrother.Models;
using BigBrother.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

//Creates a link to a testing local database for testing. This will need to be fixed once we are ready to launch the full product. 
builder.Services.AddDbContext<MonitoringContext>(options =>options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<I_InteractionsRepo, InteractionsRepo>();
builder.Services.AddScoped<I_IPAddressRepo, IPAddressRepo>();
builder.Services.AddScoped<ISessionsRepo, SessionRepo>();
builder.Services.AddScoped<IPageReferenceRepo, PageReferenceRepo>();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
