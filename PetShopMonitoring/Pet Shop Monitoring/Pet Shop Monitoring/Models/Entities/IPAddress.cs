﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pet_Shop_Monitoring.Models.Entities
{
    public class IPAddress
    {
        [Key]
        public string IpAddress { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string StateOrRegion { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public int VisitCount { get; set; }
        public string DeviceType { get; set; }
    }
}
