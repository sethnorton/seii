﻿using BigBrother.Models.Entities;
using BigBrother.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
/// <summary>
/// A namespace for the controllers of the API
/// </summary>
namespace BigBrother.Controllers
{
    /// <summary>
    /// A controller class dealing with the page reference table of the ERD
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PageReferenceController : ControllerBase
    {

        private readonly IPageReferenceRepo pageRefrenceRepo;


        /// <summary>
        /// A constructor for the page reference table
        /// </summary>
        /// <param name="pageRefrenceRepo">The interface repo instance which we are adding information</param>
        public PageReferenceController(IPageReferenceRepo pageRefrenceRepo)
        {
            this.pageRefrenceRepo = pageRefrenceRepo;
        }


        /// <summary>
        /// Method to handle a HTTP GET statement to get the ID of
        /// a specified page reference.
        /// </summary>
        /// <param name="interactionToGetID">ID of the page reference to get.</param>
        /// <returns></returns>
        [HttpGet("{pageReference}")]
        public string GetPageReference(int pageReference)
        {
            PageReferenceTable data = pageRefrenceRepo.GetEntry(pageReference.ToString());
            if (data != null)
            {
                return (data.PageId + "\n" + data.DateAdded + "\n" + data.PageDescription);
            }
            else
                return null;
            
            // Get an existing page reference within the database
        }

        /// <summary>
        /// Makes a post query to the database
        /// </summary>
        /// <param name="data">The array of strings which we want to add to the database</param>
        [HttpPost]
        public void Post(string[] data)
        {
            PageReferenceTable p = new PageReferenceTable();
            p.PageDescription = data[1];
            data[0] = data[0].Replace("%2F", "/");
            p.DateAdded = DateTime.Parse(data[0]);
          
            if (ModelState.IsValid)
            {
                pageRefrenceRepo.CreateEntry(p);
            }
        }

        /// <summary>
        /// Puts an entry into the database with a certain date and description
        /// </summary>
        /// <param name="dateAdded">The date when the page was added</param>
        /// <param name="description">The description of the page</param>
        [HttpPut("{dateAdded}/{pageDescription}")]
        public void Put(string dateAdded, string description)
        {
            PageReferenceTable p = new PageReferenceTable();
            dateAdded = dateAdded.Replace("%2F", "/");
            p.DateAdded = DateTime.Parse(dateAdded);
            p.PageDescription = description;
           pageRefrenceRepo.CreateEntry(p);
        }

        /// <summary>
        /// Deletes an id in the page reference table
        /// </summary>
        /// <param name="id">A number that visually </param>
        [HttpDelete("{id:int}")]
        public void Delete(int id)
        {
            PageReferenceTable p = new PageReferenceTable();
            p.PageId = id;
            pageRefrenceRepo.DeletePageRef(p);
        }
    }
}
