﻿using BigBrother.Models;
using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// A class dealing with storing and getting information about sessions
    /// </summary>
    public class SessionRepo : ISessionsRepo
    {

        private readonly MonitoringContext _db; //the variable dealing with getting and placing data in the database
        /// <summary>
        /// The constructor which initializes the monitoring 
        /// </summary>
        /// <param name="db">The variable we are using to change data in the database</param>
        public SessionRepo(MonitoringContext db)
        {
            _db = db;

        }

        /// <summary>
        /// Creates a new entry session in the database
        /// </summary>
        /// <param name="session">the session entry we are updating</param>
        public void CreateEntry(Session session)
        {
            _db.Session.Add(session);

            _db.SaveChanges();
        }

        /// <summary>
        /// Deletes a session in the database
        /// </summary>
        /// <param name="sessionID">Deletes a session ID</param>
        public void DeleteSessionRef(Session sessionID)
        {
            var result = GetEntry(sessionID.SessionId.ToString());
            _db.Session.Remove(sessionID);
            _db.SaveChanges();
        }

        /// <summary>
        /// Gets an entry in the database from an ID
        /// </summary>
        /// <param name="sessionID">the id of a particular session on the pet store</param>
        /// <returns></returns>
        public Session GetEntry(string sessionID)
        {
            return _db.Session.FirstOrDefault(i => i.SessionId == Int32.Parse(sessionID));
        }

        /// <summary>
        /// Updates an entry in the session controller
        /// </summary>
        /// <param name="session"></param>
        public void UpdateEntry(Session session)
        {
            var sessionEntry = GetEntry(session.SessionId.ToString());
            sessionEntry.DateTime = session.DateTime;
            sessionEntry.LoggedIn = session.LoggedIn;
            sessionEntry.PurchaseMade = session.PurchaseMade;
            _db.SaveChanges();
        }
    }
}
