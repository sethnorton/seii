﻿using System.ComponentModel.DataAnnotations;

namespace BigBrother.Models.Entities
{
    public class Session
    {   
        [Key]
        public int SessionId { get; set; }
        public int IpAddress { get; set; }
        public DateTime DateTime { get; set; }
        public bool LoggedIn { get; set; }
        public bool PurchaseMade { get; set; }

    }
}
