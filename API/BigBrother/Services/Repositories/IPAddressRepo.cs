﻿using BigBrother.Models;
using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    public class IPAddressRepo : I_IPAddressRepo
    {
        private readonly MonitoringContext _db;
        public IPAddressRepo(MonitoringContext db)
        {
            _db = db;

        }

        //Adds a new entry into the IP address table.    
        public void CreateEntry(IPAddress ipInfo)
        {
            _db.IPAddress.Add(ipInfo);
            _db.SaveChanges();  
        }

        //Pulls the row associated with an IP address from the database for use in updating. 
        public IPAddress GetEntry(string ipAddress)
        {
            //Lowkey tired of the AI telling me the primary key is nullable because its a string....
            return _db.IPAddress.FirstOrDefault(i => i.IpAddress == ipAddress);
        }

        //returns the IPAddress table as a list. This could be pointless but I will remove it later if it is. 
        public ICollection<IPAddress> ReadAll()
        {
            return _db.IPAddress.ToList();
        }
        //Updates an entry in the IP address table. To add to the count and such things. 
        public void UpdateEntry(IPAddress ipInfo)
        {
            var ipEntry = GetEntry(ipInfo.IpAddress);
            ipEntry.DeviceType = ipInfo.DeviceType;
            ipEntry.CountryCode = ipInfo.CountryCode;   
            ipEntry.City = ipInfo.City; 
            ipEntry.StateOrRegion = ipInfo.StateOrRegion;
            ipEntry.ZipCode = ipInfo.ZipCode;   
            ipEntry.VisitCount = ipEntry.VisitCount + 1;
            _db.SaveChanges();

        }
    }
}
