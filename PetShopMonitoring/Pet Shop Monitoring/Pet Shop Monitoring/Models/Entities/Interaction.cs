﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pet_Shop_Monitoring.Models.Entities
{
    public class Interaction
    {
        [Key]
        public int InteractionID { get; set; }
        [ForeignKey("Session")]
        public int SessionID { get; set; }
        public DateTime DateTime { get; set; }
        public int PageID { get; set; }
        public DateTime InteractionLength { get; set; }
    }
}
