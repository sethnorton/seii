﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BigBrother.Models.Entities
{
    /// <summary>
    /// The class declaring attributes in the page reference table in the database
    /// </summary>
    public class PageReferenceTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PageId { get; set; }
        public DateTime DateAdded { get; set; }
        public string? PageDescription { get; set; }



        /// <summary>
        /// Override for the default ToString() method.
        /// </summary>
        /// <returns>Formatted string that represents an page reference entity.</returns>
        public override string ToString()
        {
            return ("ID: " + PageId.ToString() + DateAdded.ToString()  + PageDescription.ToString()); // Return a formatted string that represents an Interaction entity
        }
    }
}
