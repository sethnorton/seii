namespace BigBrother.Services
{
    /// <summary>
    /// A generic testing interface
    /// </summary>
    public interface ITestGenericInterface
    {
        public void TestMethod<T>(T testParam); // Test generic method with generic parameter

        public T TestReturnMethod<T>(T testParam); // Test generic method with generic parameter and generic return type
    }
}