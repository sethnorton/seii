﻿using BigBrother.Models;
using BigBrother.Models.Entities;

namespace BigBrother.Services
{
    /// <summary>
    /// The class dealing with changing and grabbing data from the Interaction table in the database.
    /// </summary>
    public class InteractionsRepo : I_InteractionsRepo
    {
        private readonly MonitoringContext _db;

        /// <summary>
        /// Parameterized constructor for the InteractionRepo class.
        /// </summary>
        /// <param name="db"></param>
        public InteractionsRepo(MonitoringContext db)
        {
            _db = db;
        }

        /// <summary>
        /// Creates an Interaction entry within the database.
        /// </summary>
        /// <param name="interactionToCreate">Interaction to create an entry for.</param>
        public void CreateEntry(Interaction interactionToCreate)
        {
            _db.Interaction.Add(interactionToCreate); // Add the new entry into the database

            _db.SaveChanges(); // Save all changes made in the database
        }

        /// <summary>
        /// Updates an existing Interaction entry within the database.
        /// </summary>
        /// <param name="interactionToUpdate">Interaction entry to update.</param>
        public void UpdateEntry(Interaction interactionToUpdate)
        {
            var entryToUpdate = GetEntry(interactionToUpdate.InteractionID);

            entryToUpdate.DateTime = interactionToUpdate.DateTime;
            entryToUpdate.InteractionLength = interactionToUpdate.InteractionLength;
            _db.SaveChanges();
        }

        /// <summary>
        /// Gets an existing Interaction within the database.
        /// </summary>
        /// <param name="interactionID">ID of the Interaction to get.</param>
        /// <returns>Interaction within the database that matches the specified ID.</returns>
        public Interaction GetEntry(int interactionID)
        {
            return _db.Interaction.FirstOrDefault(i => i.InteractionID == interactionID); // Get an existing Interaction within the database where the InteractionID matches interactionID
        }

        /// <summary>
        /// Deletes an existing Interaction from the database.
        /// </summary>
        /// <param name="interactionID">ID of the Interaction to delete.</param>
        public void DeleteEntry(int interactionID)
        {
            var entryToDelete = GetEntry(interactionID); // Get the specified Interaction within the database.

            _db.Interaction.Remove(entryToDelete); // Delete the specified Interaction from the database
            _db.SaveChanges();
        }
    }
}